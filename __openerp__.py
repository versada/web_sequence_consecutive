# -*- coding: utf-8 -*-
# This file is part of OpenERP. The COPYRIGHT file at the top level of
# this module contains the full copyright notices and license terms.

{
    'name': 'Web Sequence Consecutive',
    'version': '0.1',
    'author': 'Versada UAB',
    'category': 'Hidden',
    'website': 'http://www.versada.lt',
    'description': """
Adds Consecutive Numbering for Sequences
========================================
This module makes Sequence fields act more naturally:
* First member is always #1 - default behaviour makes it possible to reorder
lines in such a way the first line might start in any number. This module
forbids that and makes sure first member is always marked 1.
* Default value is ignored for sequence field - new members always receives
next consecutive number.


    """,
    'depends': [
        'web',
    ],
    'data': [
    ],
    'update_xml': [],
    'js': ['static/src/js/*.js'],
    'installable': True,
    'application': False,
}