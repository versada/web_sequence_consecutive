openerp.web_sequence_consecutive = function(instance) {

    instance.web.ListView.Groups.include({

        setup_resequence_rows: function (list, dataset) {
            // drag and drop enabled if list is not sorted and there is a
            // visible column with @widget=handle or "sequence" column in the view.
            if ((dataset.sort && dataset.sort())
                || !_(this.columns).any(function (column) {
                        return column.widget === 'handle'
                            || column.name === 'sequence'; })) {
                return;
            }
            var sequence_field = _(this.columns).find(function (c) {
                return c.widget === 'handle';
            });
            var seqname = sequence_field ? sequence_field.name : 'sequence';

            // ondrop, move relevant record & fix sequences
            list.$current.sortable({
                axis: 'y',
                items: '> tr[data-id]',
                helper: 'clone'
            });
            if (sequence_field) {
                list.$current.sortable('option', 'handle', '.oe_list_field_handle');
            }
            list.$current.sortable('option', {
                start: function (e, ui) {
                    ui.placeholder.height(ui.item.height());
                },
                stop: function (event, ui) {
                    var to_move = list.records.get(ui.item.data('id')),
                        target_id = ui.item.prev().data('id'),
                        from_index = list.records.indexOf(to_move),
                        target = list.records.get(target_id);
                    if (list.records.at(from_index - 1) == target) {
                        return;
                    }

                    list.records.remove(to_move);
                    var to = target_id ? list.records.indexOf(target) + 1 : 0;
                    list.records.add(to_move, { at: to });

                    // resequencing time!
                    var record, index = 0,
                        // if drag to 1st row (to = 0), start sequencing from 0
                        // (exclusive lower bound)
                        seq = 0;
                    while (++seq, record = list.records.at(index++)) {
                        // write are independent from one another, so we can just
                        // launch them all at the same time and we don't really
                        // give a fig about when they're done
                        // FIXME: breaks on o2ms (e.g. Accounting > Financial
                        //        Accounting > Taxes > Taxes, child tax accounts)
                        //        when synchronous (without setTimeout)
                        (function (dataset, id, seq) {
                            $.async_when().done(function () {
                                var attrs = {};
                                attrs[seqname] = seq;
                                dataset.write(id, attrs);
                            });
                        }(dataset, record.get('id'), seq));
                        record.set(seqname, seq);
                    }
                }
            });
        },
    });

    instance.web.DataSet.include({
        default_get: function(fields, options) {
            var to_ret = this._super(fields, options);
            to_ret.next_id = this.ids ? this.ids.length + 1: 1;
            if (_.include(fields, "sequence")) {
                to_ret.done(function(res) {
                    res.sequence = this.next_id;
                });
            }
            return to_ret;
        },
    });
};
